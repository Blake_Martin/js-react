import {standardHeaders, sessionUrl} from '../../../utilities/url-builder';
import {push} from 'react-router-redux';

const UPDATE = `reactProject/portal/session/UPDATE`;

export default (state = {}, action = {}) => {
    return action.type === UPDATE ? action.payload : state;
}

const updateUserSession = session => {
    return {
        type: UPDATE,
        payload: session
    };
};

export const fetchSession = () => {
    return (dispatch, getState) => {
        const {properties, authentication} = getState();

        fetch(sessionUrl(properties.portalUrl), {
            method: 'get',
            headers: standardHeaders(authentication.accessToken)
        }).then(response => {
            if (!response.ok) {
                if (response.status === 401) {
                    dispatch(push('/login'));
                }
                else {
                    throw new Error('Session endpoint errored');
                }
            } else {
                response
                    .json()
                    .then(responseJson => {
                        const clientName = responseJson.identity.clientName;
                        const username = responseJson.identity.username;

                        if (window.appInsights) {
                            window.appInsights.setAuthenticatedUserContext(username, clientName);
                        }

                        dispatch(updateUserSession(responseJson));
                        dispatch(push('/'));
                    });
            }
        }).catch(() => {
            dispatch(push('/error'));
        });
    };
};

export const updateSession = session => {
    return (dispatch, getState) => {
        const {properties, authentication} = getState();

        dispatch(updateUserSession(session));

        fetch(sessionUrl(properties.portalUrl), {
            method: 'put',
            headers: standardHeaders(authentication.accessToken),
            body: JSON.stringify(session)
        }).then(response => {
            if (response.status === 401) {
                dispatch(push('/login'));
            }
        });
    };
};